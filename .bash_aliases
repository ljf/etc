#!/bin/bash

# Quick cd
alias cdynh='cd ~/Documents/Code/yunohost'
alias cdrl='cd ~/Documents/Code/reflexlibre'
alias cdarn='cd ~/Documents/Code/arn'

# Quick full-upgrade
alias upgrade='sudo apt update; sudo apt full-upgrade'

# Test
alias isup='ps aux | grep '

