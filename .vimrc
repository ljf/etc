set nocompatible              " required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Add all your plugins here (note older versions of Vundle used Bundle
" instead of Plugin)
Bundle 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Bundle 'scrooloose/nerdtree'
" Bundle 'jistr/vim-nerdtree-tabs'
Bundle 'Xuyuanp/nerdtree-git-plugin'

Bundle 'ycm-core/YouCompleteMe'
" Bundle 'klen/python-mode'
Bundle 'scrooloose/syntastic'
" Bundle 'pyflakes.vim'
Bundle 'tmhedberg/SimpylFold'
Bundle 'vim-scripts/indentpython.vim'
"Bundle 'nvie/vim-flake8'
Bundle 'kien/ctrlp.vim'
Bundle 'tpope/vim-fugitive'
Bundle 'preservim/tagbar'
Bundle 'mhinz/vim-signify'
Bundle 'bling/vim-airline'
Bundle 'vim-airline/vim-airline-themes'

" Color TOML
Bundle 'cespare/vim-toml'
Bundle 'maralla/vim-toml-enhance'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" #########################################
" ##   NERDTree                          ##
" #########################################

map <F2> :NERDTreeToggle<CR>
"autocmd vimenter * NERDTree " automatically open NERDTree at startup
let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Modified'  :'✹',
                \ 'Staged'    :'✚',
                \ 'Untracked' :'✭',
                \ 'Renamed'   :'➜',
                \ 'Unmerged'  :'═',
                \ 'Deleted'   :'✖',
                \ 'Dirty'     :'✗',
                \ 'Ignored'   :'☒',
                \ 'Clean'     :'✔︎',
                \ 'Unknown'   :'?',
                \ }
let NERDTreeIgnore=['\.pyc$', '\~$'] "ignore files in NERDTree
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
"let NERDTreeMapOpenInTab='<TAB>'
" Start NERDTree. If a file is specified, move the cursor to its window.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * NERDTree | if argc() > 0 || exists("s:std_in") | wincmd p | endif
" Exit Vim if NERDTree is the only window left.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() |
    \ quit | endif
" Open the existing NERDTree on each new tab.
autocmd BufWinEnter * silent NERDTreeMirror

" #########################################
" ##   VIM Airline                       ##
" #########################################

set laststatus=2 " always activate airline bar
let g:airline_powerline_fonts = 1 " activate powerline fonts (github.com/powerline/fonts) if installed
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='wombat'
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

" #########################################
" ##   Flake / PEP 8                     ##
" #########################################

" autocmd BufWritePost *.py call Flake8()
" let g:syntastic_python_checkers = ['flake8', 'pep8']
let g:flake8_config_file="~/.flake8"
let g:flake8_show_in_gutter=1
let g:flake8_show_in_file=1
let g:flake8_show_quickfix=0

let g:flake8_error_marker='EE'     " set error marker to 'EE'
let g:flake8_warning_marker='WW'   " set warning marker to 'WW'

" to use colors defined in the colorscheme
highlight link Flake8_Error      Error
highlight link Flake8_Warning    WarningMsg
highlight link Flake8_Complexity WarningMsg
highlight link Flake8_Naming     WarningMsg
highlight link Flake8_PyFlake    WarningMsg


set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_python_checkers=['flake8']
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 0
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_loc_list_height=3

" #########################################
" ##   Auto completion                   ##
" #########################################
let g:ycm_min_num_of_chars_for_completion = 3
let g:ycm_auto_trigger = 1

" #########################################
" ##   TagBar                            ##
" #########################################

nmap <F8> :TagbarToggle<CR>

" #########################################
" ##   Regular vim configuration         ##
" #########################################

setlocal spelllang=fr

" Jump to the last position when reopening a file
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" Load indentation rules and plugins according to detected filetype
filetype indent on
filetype plugin on
let g:tex_flavor='latex'
let g:tex_indent_brace=0

" Editor option
set showcmd             " Show command in status line
set showmatch           " Show matching brackets.
set number              " Enable numbers in front of lines
set mouse=a             " Enable mouse usage (all modes)
set autoindent          " Automatic indentation
set shiftwidth=4        " Set tab width to 4 characters
set expandtab           " Replace tab character by spaces

" Search
set incsearch           " Incremental search
set hlsearch            " Enable highlighted words in incremental search.
set ignorecase          " Do case insensitive matching
set smartcase           " Do smart case matching

" Stuff
set autowrite           " Automatically save before commands like :next and :make
set hidden              " Hide buffers when they are abandoned
set scrolloff=2         " Set visible lines over or under the block
set noswapfile
set tabstop=4
set linebreak
set autochdir           " Changer automatiquement de repertoire de travail.

au FileType py set textwidth=79 " PEP-8 Friendly
au FileType py set colorcolumn=79      " Display limit at column=90
highlight OverLength ctermbg=red ctermfg=white guibg=#592929
match OverLength /\%81v.\+/

" Syntax highlighting
syntax on               " Syntax highlighting
set background=dark     " If using dark background

" if &t_Co >= 256
"     colorscheme monokai
" else
"     colorscheme elflord
" endif
" Adding .pde as cpp files (for Arduino dev)
au BufNewFile,BufRead *.pde set filetype=cpp

" Set encoding to UTF8
set encoding=utf-8

" Toggle pasting mode
set pastetoggle=<F10>

" Paste by default in system clipboard (vim-gtk needed)
set clipboard=unnamedplus

" Automatically remove trailing spaces in some files
function! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
endfun
autocmd BufWritePre *.C   : call <SID>StripTrailingWhitespaces()
autocmd BufWritePre *.c   : call <SID>StripTrailingWhitespaces()
autocmd BufWritePre *.cpp : call <SID>StripTrailingWhitespaces()
autocmd BufWritePre *.h   : call <SID>StripTrailingWhitespaces()
autocmd BufWritePre *.tex : call <SID>StripTrailingWhitespaces()
autocmd BufWritePre *.py  : call <SID>StripTrailingWhitespaces()

" Fix backspace key
" fixdel

" Source a global configuration file if available
"if filereadable("/etc/vim/vimrc.local")
"  source /etc/vim/vimrc.local
"endif

let python_highlight_all=1
syntax on
" set tabs to have 4 spaces
set ts=4
" indent when moving to the next line while writing code
au FileType py set smartindent
" when using the >> or << commands, shift lines by 4 spaces
set shiftwidth=4
" show a visual line under the cursor's current line
"set cursorline

set softtabstop=4 " insert/delete 4 spaces when hitting a TAB/BACKSPACE
set shiftround    " round indent to multiple of 'shiftwidth'
set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ 9
set laststatus=2

set foldmethod=indent
set foldlevel=99
let g:SimpylFold_docstring_preview=1
let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>


autocmd StdinReadPre * let s:std_in=1
set backspace=indent,eol,start
